/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.manastria.prog_tv;

/**
 *
 * @author benoit
 */
public class Programme {
    public String Type;
    public Integer Horaire;
    public Integer Duree;
    public String Nom;

    Programme(String Type, Integer Horaire, Integer Duree, String Nom, String Theme) {
        this.Type=Type;
        this.Horaire=Horaire;
        this.Duree=Duree;
        this.Nom=Nom;
        Reportage.Theme=Theme;
        
    }
    
     Programme(String Type, Integer Horaire, Integer Duree, String Nom, Integer annee, String realisateur, Boolean redif) {
        this.Type=Type;
        this.Horaire=Horaire;
        this.Duree=Duree;
        this.Nom=Nom;
        Fiction.annee=annee;
        Fiction.realisateur=realisateur;
        Fiction.redif=redif;
        
    }
     
      Programme(String Type, int Horaire, String Nom, String annimateur) {
        this.Type=Type;
        this.Horaire=Horaire;
        this.Duree=2;
        this.Nom=Nom;
        Divertissement.annimateur=annimateur;
        
    }
    
    public String toString()
    {
        if (Type=="reportage") 
        {
            return ("Programme [type programme : "+ Type + " | Horraires : " + Horaire + " | Durée : " + Duree + " | Nom : " + Nom + " | Theme : "+ Reportage.Theme +" ]");
        }
        
        else 
        { 
        if (Type=="divertissement")
        {
        return ("Programme [type programme : "+ Type + " | Horraires : " + Horaire + " | Durée : " + Duree + " | Nom : " + Nom + " | annimateur : "+ Divertissement.annimateur +" ]");
        }
        else
            {
            return ("Programme [type programme : "+ Type + " | Horraires : " + Horaire + " | Durée : " + Duree + " | Nom : " + Nom + " | annee : "+ Fiction.annee + " | realisateur : "+ Fiction.realisateur + " | rediffusion ? " + Fiction.redif + " ]");
            }
        }
        
        
        
        
       
    }


        
        
        
        
}
